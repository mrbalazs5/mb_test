<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\User;
use App\Form\LoginFormType;

class UserController extends Controller
{
    /**
     * @Route("/", name="login")
     */
    public function Login(Request $request)
    {

        $session = $request->getSession();

        if(!empty($session->get('user'))){
          return $this->redirectToRoute('main');
        }

        $user = new User();

        $form = $this->createForm(LoginFormType::class, $user);
        $tries = 0;

        $form->handleRequest($request);

        if($form->isSubmitted()){
          if($session->has('tries') && ($session->get('tries') > 3)){
            if($form->isValid() && $this->captchaverify($request->get('g-recaptcha-response')) && $this->dbValidate($form, $session)){

              return $this->redirectToRoute('main');

            }
          }else{
            if($form->isValid() && $this->dbValidate($form, $session)){

              return $this->redirectToRoute('main');

            }
          }

          if(!$session->has('tries')){
            $session->set('tries', 1);
            $tries = $session->get('tries');
          }else{
              $session->set('tries', $session->get('tries') + 1);
              $tries = $session->get('tries');
          }
        }


        return $this->render('user/login.html.twig', ['form' => $form->createView(), 'tries' => $tries]);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout(Request $request){
      $session = $request->getSession();

      if(empty($session->get('user'))){
        return $this->redirectToRoute('login');
      }

      $session->invalidate();

      return $this->redirectToRoute('login');
    }


    private function captchaverify($recaptcha){
           $url = "https://www.google.com/recaptcha/api/siteverify";
           $ch = curl_init();
           curl_setopt($ch, CURLOPT_URL, $url);
           curl_setopt($ch, CURLOPT_HEADER, 0);
           curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
           curl_setopt($ch, CURLOPT_POST, true);
           curl_setopt($ch, CURLOPT_POSTFIELDS, [
               "secret"=>"6LemMGEUAAAAAABICVFOK0ZgoA_eW208uyfepUg-","response"=>$recaptcha]);
           $response = curl_exec($ch);
           curl_close($ch);
           $data = json_decode($response);
           //teszt miatt
           $data = true;
           //amúgy return $data->success;

       return $data;
   }

   private function dbValidate($form, $session){

     $repository = $this->getDoctrine()->getRepository(User::class);
     $entityManager = $this->getDoctrine()->getManager();

     if(!empty($db_user = $repository->findOneBy(['username' =>  $form["username"]->getData()]))){
       if(password_verify( $form["password"]->getData(), $db_user->getPassword())){

         $db_user->setLastLogin(new \DateTime);
         $entityManager->flush();

         $session->invalidate();
         $session->set('user', $db_user);

         return true;

       }else{
         $this->addFlash('login_err', 'Can not find password in database!');
       }
     }else{
       $this->addFlash('login_err', 'Can not find username in database!');
     }

     return false;
   }

}
