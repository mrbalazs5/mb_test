<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class MainController extends Controller
{
    /**
     * @Route("/main", name="main")
     */
    public function mainPage(Request $request)
    {

      if($this->check_login($request)){
        $session = $request->getSession();
        $user = $session->get('user');

        return $this->render('main/main.html.twig', ['user' => $user]);
      }

      return $this->redirectToRoute('perm_den');
    }

    /**
     * @Route("/profile", name="profile")
     */
    public function profile(Request $request)
    {
      if($this->check_login($request)){

        if($this->is_user($request) || $this->is_admin($request)){
          $session = $request->getSession();
          $user = $session->get('user');

          return $this->render('user/profile.html.twig', ['user' => $user]);
        }

      }

      return $this->redirectToRoute('perm_den');

    }

    /**
     * @Route("/content", name="content")
     */
    public function contentManager(Request $request)
    {
      if($this->check_login($request)){

        if($this->is_editor($request) || $this->is_admin($request)){
          $session = $request->getSession();
          $user = $session->get('user');

          return $this->render('user/content.html.twig', ['user' => $user]);
        }

      }

      return $this->redirectToRoute('perm_den');

    }

    /**
     * @Route("/admin", name="admin")
     */
    public function admin(Request $request)
    {
      if($this->check_login($request)){

        if($this->is_admin($request)){
          $session = $request->getSession();
          $user = $session->get('user');

          return $this->render('admin/admin.html.twig', ['user' => $user]);
        }

      }

      return $this->redirectToRoute('perm_den');

    }

    /**
     * @Route("/permission-denied", name="perm_den")
     */
    public function permission_denied(){

      return $this->render('error/perm_den.html.twig');
    }



    private function check_login(Request $request){
      $session = $request->getSession();

      if(!$session->has('user')){
        return false;
      }

      return true;
    }

    private function is_admin(Request $request){
      $session = $request->getSession();
      $user = $session->get('user');

      if(in_array('ROLE_ADMIN', $user->getRoles())){
        return true;
      }
        return false;
    }

    private function is_editor(Request $request){
      $session = $request->getSession();
      $user = $session->get('user');

      if(in_array('ROLE_EDITOR', $user->getRoles())){
        return true;
      }
        return false;
    }

    private function is_user(Request $request){
      $session = $request->getSession();
      $user = $session->get('user');

      if(in_array('ROLE_USER', $user->getRoles())){
        return true;
      }
        return false;
    }

}
