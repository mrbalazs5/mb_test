<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SessionsRepository")
 */
class Sessions
{
    /**
     * @ORM\Column(type="string", length=128, nullable=false)
     * @ORM\Id
     */
    private $sess_id;

    /**
     * @ORM\Column(type="blob", nullable=false)
     */
    private $sess_data;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $sess_time;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $sess_lifetime;

    public function getId()
    {
        return $this->id;
    }
}
