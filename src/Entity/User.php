<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="username", type="string", length=255, unique=true, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 3,
     *      max = 10,
     *      minMessage="Your username must be at least 3 characters long!",
     *      maxMessage="Your username can not be longer than 10 characters!"
     *)
     **/
    private $username;

    /**
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 6,
     *      minMessage="Your password must be at least 6 characters long!"
     *)
     **/
    private $password;

    /**
     * @ORM\Column(type="simple_array")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="datetime")
     */
    private $lastLogin;

    public function getId()
    {
        return $this->id;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function setRoles($roles)
    {
        $this->roles = $roles;
    }

    public function getLastLogin(){
      return $this->lastLogin;
    }

    public function setLastLogin($last){
      $this->lastLogin = $last;
    }

   public function serialize()
   {
       return serialize(array(
           $this->id,
           $this->username,
           $this->password,
       ));
   }

   public function unserialize($serialized)
   {
       list (
           $this->id,
           $this->username,
           $this->password,
       ) = unserialize($serialized, ['allowed_classes' => false]);
   }

}
