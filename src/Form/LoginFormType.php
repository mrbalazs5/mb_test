<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class LoginFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, ['label' => 'Username',
                                                'required' => true,
                                                'attr' => ['class' => 'form-control']
                                               ])
            ->add('password', PasswordType::class, ['label' => 'Password',
                                                    'required' => true,
                                                    'attr' => ['class' => 'form-control']])
            ->add('save', SubmitType::class, ['label' => 'Login',
                                              'attr' => ['class' => 'btn btn-primary mb-2']])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
