<?php
namespace App\DataFixtures\ORM;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;


class LoadUserData extends Fixture{

  public function load(ObjectManager $manager){
    $user = new User();
    $user->setUsername('Admin');
    $user->setPassword(password_hash('admin_pass', PASSWORD_BCRYPT));
    $user->setRoles(['ROLE_ADMIN']);
    $user->setLastLogin(new \DateTime());

    $manager->persist($user);
    $manager->flush();

    $user = new User();
    $user->setUsername('User 1');
    $user->setPassword(password_hash('user1_pass', PASSWORD_BCRYPT));
    $user->setRoles(['ROLE_EDITOR', 'ROLE_USER']);
    $user->setLastLogin(new \DateTime());

    $manager->persist($user);
    $manager->flush();

    $user = new User();
    $user->setUsername('User 2');
    $user->setPassword(password_hash('user2_pass', PASSWORD_BCRYPT));
    $user->setRoles(['ROLE_EDITOR']);
    $user->setLastLogin(new \DateTime());

    $manager->persist($user);
    $manager->flush();

    $user = new User();
    $user->setUsername('User 3');
    $user->setPassword(password_hash('user3_pass', PASSWORD_BCRYPT));
    $user->setRoles(['ROLE_USER']);
    $user->setLastLogin(new \DateTime());

    $manager->persist($user);
    $manager->flush();
  }

}
