Teszt, Megyeri Balázs, Straxus
 
 Installáció:
 
 composer install
 
 adatbázis konfig, .env stb.
 
 php bin/console doctrine:database:create
 
 php bin/console doctrine:migrations:migrate
 
 php bin/console doctrine:fixtures:load
 
 php -S 127.0.0.1:8000 -t public / saját host, akármi
 
 Felhasználók:
 
 Felhasználónév:
 Admin
 Jelszó:
 admin_pass
 
 Felhasználónév:
 User 1
 Jelszó:
 user1_pass
 
 Felhasználónév:
 User 2
 Jelszó:
 user2_pass
 
 Felhasználónév:
 User 3
 Jelszó:
 user3_pass